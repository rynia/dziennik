package com.school.register.dataaccess;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class Grade {

    @Id
    private Long id;

    @Column(nullable = false)
    private Float value;

    @Column(nullable = false)
    private LocalDateTime setAt;

    @Column(nullable = false)
    private LocalDateTime lastModified;

    @OneToOne(optional = false)
    private Teacher teacher;

    @OneToOne(optional = false)
    private Student student;

    @OneToOne(optional = false)
    private Subject subject;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Comment comment;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public LocalDateTime getSetAt() {
        return setAt;
    }

    public void setSetAt(LocalDateTime setAt) {
        this.setAt = setAt;
    }

    public LocalDateTime getLastModified() {
        return lastModified;
    }

    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Subject getSubject() {
        return subject;
    }

    public void setSubject(Subject subject) {
        this.subject = subject;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }
}
