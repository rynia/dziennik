package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.SchoolClass;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SchoolClassRepository extends CrudRepository<SchoolClass, Long> {
}
