package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.Student;
import com.school.register.logic.rest.to.PersonView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {

    @Query(value = "select student.id, concat(personal_data.first_name, ' ', personal_data.last_name) as name\n" +
            "from student\n" +
            "join personal_data on student.personal_data_id  = personal_data.id", nativeQuery = true)
    Collection<PersonView> getAllStudents();

}
