package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.Grade;
import com.school.register.logic.rest.to.GradeView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface GradeRepository extends CrudRepository<Grade, Long> {

    @Query(value = "select g.id, g.value as value, subject.name as subject, concat(student_personal.first_name, ' ', student_personal.last_name) as student, \n" +
            "concat(teacher_personal.first_name, ' ', teacher_personal.last_name) as teacher, comm.content as comment, g.set_at as setAt, \n" +
            "comm.id as commentId, comm.author_id as commentAuthorId \n" +
            "from grade g \n" +
            "join subject on g.subject_id = subject.id \n" +
            "join student on g.student_id = student .id \n" +
            "join personal_data student_personal on student.id = student_personal.id \n" +
            "join teacher on g.teacher_id = teacher.id \n" +
            "join personal_data teacher_personal on teacher.personal_data_id = teacher_personal.id \n" +
            "join comment comm on g.comment_id = comm.id", nativeQuery = true)
    Collection<GradeView> findAllGrades();

}
