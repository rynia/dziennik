package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.Teacher;
import com.school.register.logic.rest.to.PersonView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface TeacherRepository extends CrudRepository<Teacher, Long> {

    @Query(value = "select teacher.id, concat(personal_data.first_name, ' ', personal_data.last_name) as name \n" +
            "from teacher\n" +
            "join personal_data on teacher.personal_data_id = personal_data.id", nativeQuery = true)
    Collection<PersonView> getAllTeachers();
}
