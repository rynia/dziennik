package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.Subject;
import com.school.register.logic.rest.to.SubjectView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SubjectRepository extends CrudRepository<Subject, Long> {

    @Query(value = "select subject.id, subject.name \n" +
            "from subject", nativeQuery = true)
    Collection<SubjectView> getAllSubjects();
}
