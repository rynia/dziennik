package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.TimeTableRow;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TimeTableRowRepository extends CrudRepository<TimeTableRow, Long> {
}
