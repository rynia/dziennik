package com.school.register.dataaccess.repository;

import com.school.register.dataaccess.BadReportCard;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BadReportCardRepository extends CrudRepository<BadReportCard, Long> {
}
