package com.school.register.dataaccess;

import javax.persistence.*;


@MappedSuperclass
public abstract class AbstractPerson {

    @Id
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, optional = false)
    private PersonalData personalData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonalData getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }
}
