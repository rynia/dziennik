package com.school.register.dataaccess;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
public class BadReportCard {

    @Id
    private Long id;

    @Column(nullable = false)
    private LocalDateTime setAt;

    @Column(nullable = false)
    private boolean seenByParents;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Comment comment;

    @OneToOne(optional = false)
    private Student student;

    @OneToOne(optional = false)
    private Teacher teacher;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getSetAt() {
        return setAt;
    }

    public void setSetAt(LocalDateTime setAt) {
        this.setAt = setAt;
    }

    public boolean isSeenByParents() {
        return seenByParents;
    }

    public void setSeenByParents(boolean seenByParents) {
        this.seenByParents = seenByParents;
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }
}
