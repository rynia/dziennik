package com.school.register.dataaccess;

import com.school.register.dataaccess.common.WeekDayISOtoDayOfWeekConverter;

import javax.persistence.*;
import java.time.DayOfWeek;
import java.time.LocalTime;

@Entity
public class TimeTableRow {

    @Id
    private Long id;

    @Column(nullable = false)
    @Convert(converter = WeekDayISOtoDayOfWeekConverter.class)
    private DayOfWeek weekDayIso;

    @Column(nullable = false)
    private LocalTime time;

    @OneToOne(optional = false)
    private SchoolClass schoolClass;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DayOfWeek getWeekDayIso() {
        return weekDayIso;
    }

    public void setWeekDayIso(DayOfWeek weekDayIso) {
        this.weekDayIso = weekDayIso;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public SchoolClass getSchoolClass() {
        return schoolClass;
    }

    public void setSchoolClass(SchoolClass schoolClass) {
        this.schoolClass = schoolClass;
    }
}
