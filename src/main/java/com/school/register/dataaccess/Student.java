package com.school.register.dataaccess;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class Student extends AbstractPerson {

    @ManyToOne(optional = false)
    private PersonalData personalData;

    @OneToOne(optional = false)
    private Parent parent;


    @Override
    public PersonalData getPersonalData() {
        return personalData;
    }

    @Override
    public void setPersonalData(PersonalData personalData) {
        this.personalData = personalData;
    }

    public Parent getParent() {
        return parent;
    }

    public void setParent(Parent parent) {
        this.parent = parent;
    }
}
