package com.school.register.logic.rest.to;

import java.time.LocalDateTime;

public interface GradeView {

    public Long getId();

    public Float getValue();

    public String getSubject();

    public String getStudent();

    public String getTeacher();

    public String getComment();

    public Long getCommentId();

    public Long getCommentAuthorId();

    public LocalDateTime getSetAt();

}
