package com.school.register.logic.rest.to;

public interface PersonView {

    String getId();

    String getName();
}
