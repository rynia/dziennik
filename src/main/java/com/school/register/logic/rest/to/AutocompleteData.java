package com.school.register.logic.rest.to;

import java.util.Collection;

public class AutocompleteData {

    private Collection<PersonView> teachers;
    private Collection<PersonView> students;
    private Collection<SubjectView> subjects;

    public AutocompleteData(Collection<PersonView> teachers, Collection<PersonView> students, Collection<SubjectView> subjects) {
        this.teachers = teachers;
        this.students = students;
        this.subjects = subjects;
    }

    public Collection<PersonView> getTeachers() {
        return teachers;
    }

    public void setTeachers(Collection<PersonView> teachers) {
        this.teachers = teachers;
    }

    public Collection<PersonView> getStudents() {
        return students;
    }

    public void setStudents(Collection<PersonView> students) {
        this.students = students;
    }

    public Collection<SubjectView> getSubjects() {
        return subjects;
    }

    public void setSubjects(Collection<SubjectView> subjects) {
        this.subjects = subjects;
    }
}
