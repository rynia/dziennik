package com.school.register.logic.rest.to;

public interface SubjectView {

    Long getId();
    String getName();
}
