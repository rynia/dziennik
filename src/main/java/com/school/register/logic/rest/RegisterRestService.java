package com.school.register.logic.rest;

import com.school.register.dataaccess.*;
import com.school.register.logic.rest.to.AutocompleteData;
import com.school.register.logic.rest.to.GradeTo;
import com.school.register.logic.rest.to.GradeView;
import com.school.register.logic.service.AutocompleteService;
import com.school.register.logic.service.GradeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.Collection;

@RestController
public class RegisterRestService {

    @Autowired
    private GradeService gradeService;

    @Autowired
    private AutocompleteService autocompleteService;

    @GetMapping("grade")
    @ResponseBody
    public Collection<GradeView> getGrades() {
        return gradeService.getGrades();
    }

    @PostMapping("grade")
    @ResponseBody
    public Grade saveGrade(@RequestBody GradeTo to) {

        Grade grade = mapGradeTOtoGrade(to);
        return gradeService.saveGrade(grade);
    }

    @GetMapping("autocomplete")
    @ResponseBody
    public AutocompleteData getAutocompleteData() {
        return autocompleteService.getAutocompleteData();
    }

    private Grade mapGradeTOtoGrade(GradeTo gradeTo) {
        Grade grade = new Grade();
        grade.setId(gradeTo.getId());
        grade.setLastModified(LocalDateTime.now());
        grade.setSetAt(gradeTo.getSetAt());
        grade.setValue(gradeTo.getValue());

        Student student = new Student();
        student.setId(gradeTo.getStudentId());
        grade.setStudent(student);

        Teacher teacher= new Teacher();
        teacher.setId(gradeTo.getTeacherId());
        grade.setTeacher(teacher);

        Subject subject =  new Subject();
        subject.setId(gradeTo.getSubjectId());
        grade.setSubject(subject);

        Comment comment = new Comment();
        comment.setId(gradeTo.getCommentId());
        comment.setContent(gradeTo.getComment());
        comment.setCreatedAt(gradeTo.getSetAt());

        PersonalData personalData = new PersonalData();
        personalData.setId(gradeTo.getCommentAuthorId());
        comment.setAuthor(personalData);

        grade.setComment(comment);

        return grade;
    }

}
