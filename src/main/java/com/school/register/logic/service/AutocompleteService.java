package com.school.register.logic.service;

import com.school.register.dataaccess.repository.StudentRepository;
import com.school.register.dataaccess.repository.SubjectRepository;
import com.school.register.dataaccess.repository.TeacherRepository;
import com.school.register.logic.rest.to.AutocompleteData;
import com.school.register.logic.rest.to.PersonView;
import com.school.register.logic.rest.to.SubjectView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class AutocompleteService {

    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;


    public AutocompleteData getAutocompleteData() {
        Collection<PersonView> teachers = teacherRepository.getAllTeachers();
        Collection<PersonView> students = studentRepository.getAllStudents();
        Collection<SubjectView> subjects = subjectRepository.getAllSubjects();

        return new AutocompleteData(teachers, students, subjects);
    }

}
