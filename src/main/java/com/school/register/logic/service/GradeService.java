package com.school.register.logic.service;

import com.google.common.collect.Lists;
import com.school.register.dataaccess.Grade;
import com.school.register.dataaccess.repository.CommentRepository;
import com.school.register.dataaccess.repository.GradeRepository;
import com.school.register.logic.rest.to.GradeView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.Collection;

@Component
public class GradeService {

    @Autowired
    private GradeRepository repository;


    public Collection<GradeView> getGrades() {
        return repository.findAllGrades();
    }

    @Transactional
    public Grade saveGrade(Grade grade) {
        return repository.save(grade);
    }

}
