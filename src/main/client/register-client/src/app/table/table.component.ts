import {Component, OnInit, ViewChild} from '@angular/core';
import {InputDef} from '../table-editor/input-def';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {AutocompleteData, GradeTo, GradeView, RestService} from "../rest.service";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  selected: GradeView;
  dataSource: MatTableDataSource<GradeView>;
  grades: GradeView[];
  columnsToDisplay: string[] = ['student', 'subject', 'value', 'teacher', 'comment', 'setAt'];
  editableFields: InputDef[] = [
    {fieldName: 'student', inputLabel: 'Uczeń', type: 'autocomplete', options: []},
    {fieldName: 'subject', inputLabel: 'Przedmiot', type: 'autocomplete', options: []},
    {fieldName: 'value', inputLabel: 'Ocena', type: 'string'},
    {fieldName: 'teacher', inputLabel: 'Nauczyciel', type: 'autocomplete', options: []},
    {fieldName: 'comment', inputLabel: 'Komentarz', type: 'string'},
    {fieldName: 'setAt', inputLabel: 'Wystawiono', type: 'date'}
  ];

  @ViewChild(MatSort, {static: true}) sort: MatSort;
  private autocompleteData: AutocompleteData;

  constructor(private restService: RestService) {
  }

  ngOnInit() {
    this.restService.getGrades().subscribe(grades => {
      this.grades = grades;
      this.dataSource = new MatTableDataSource(this.grades);
      this.dataSource.sort = this.sort;
    });

    this.restService.getAutocompleteData().subscribe(data => {
      this.autocompleteData = data;
      this.editableFields.find(field => field.fieldName === 'student').options = this.autocompleteData.students.map(student => student.name);
      this.editableFields.find(field => field.fieldName === 'teacher').options = this.autocompleteData.teachers.map(teacher => teacher.name);
      this.editableFields.find(field => field.fieldName === 'subject').options = this.autocompleteData.subjects.map(subject => subject.name);
    });
  }

  setSelected(element: GradeView) {
    if (this.selected === element) {
      this.selected = null;
    } else {
      this.selected = element;
    }
  }

  onEditionFinished(event: GradeView) {
    if (this.selected) {
      Object.assign(this.selected, event);
    } else {
      this.grades.push(event);
      this.dataSource = new MatTableDataSource(this.grades);
    }
    const toSave = this.mapGradeViewToGradeTo(event);
    this.restService.saveGrade(toSave).subscribe(saved => console.log(saved));
  }

  private mapGradeViewToGradeTo(gradeView: GradeView): GradeTo {

    return {
      id: gradeView.id,
      lastModified: new Date(),
      value: gradeView.value,
      teacherId: this.autocompleteData.teachers.find(teacher => teacher.name === gradeView.teacher).id,
      studentId: this.autocompleteData.students.find(student => student.name === gradeView.student).id,
      subjectId: this.autocompleteData.subjects.find(subject => subject.name === gradeView.subject).id,
      comment: gradeView.comment,
      commentId: gradeView.commentId,
      commentAuthorId: gradeView.commentAuthorId,
      setAt: gradeView.setAt
    } as GradeTo;

  }
}
