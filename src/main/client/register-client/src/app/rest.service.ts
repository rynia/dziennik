import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private httpClient: HttpClient) {
  }

  public getGrades(): Observable<GradeView[]> {
    return this.httpClient.get<GradeView[]>("/api/grade");
  }

  public saveGrade(grade: GradeTo) {
    return this.httpClient.post("/api/grade", grade);
  }

  public getAutocompleteData(): Observable<AutocompleteData> {
    return this.httpClient.get<AutocompleteData>("/api/autocomplete");
  }
}

export interface GradeTo {
  id: number,
  value: number,
  setAt: Date,
  lastModified: Date;
  teacherId: number,
  studentId: number,
  subjectId: number,
  comment: string,
  commentId: number,
  commentAuthorId: number
}

export interface GradeView {
  id: number,
  value: number,
  subject: string,
  teacher: string,
  student: string,
  comment: string,
  commentId: number,
  commentAuthorId: number,
  setAt: Date
}

export interface CommentView {

}

export interface PersonTo {
  id: number,
  name: string
}

export interface SubjectTo {
  id: number,
  name: string
}

export interface AutocompleteData {
  teachers: PersonTo[],
  students: PersonTo[],
  subjects: SubjectTo[]
}
