import {Component, OnInit} from '@angular/core';
import {GradeView, RestService} from "./rest.service";
import {Observable} from "rxjs";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'register-client';

  constructor(private restService: RestService) {
  }

  ngOnInit() {
  }
}
