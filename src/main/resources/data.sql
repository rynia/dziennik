-- Initial data
insert into personal_data values (1, 'Marek', 'Brzozowski', '777 000 999');
insert into personal_data values (2, 'Tomasz', 'Brzozowski', '777 001 999');

insert into personal_data values (3, 'Zbigniew', 'Zborewicz', '777 002 999');
insert into personal_data values (4, 'Lucja', 'Zborewicz', '777 003 999');

insert into personal_data values (5, 'Paulina', 'Bandurska', '777 004 999');
insert into personal_data values (6, 'Julia', 'Bandurska', '777 005 999');

insert into personal_data values (7, 'Anna', 'Ziemowicz', '777 006 999');
insert into personal_data values (8, 'Barbara', 'Ziemowicz', '777 007 999');

insert into personal_data values (10, 'Zuzanna', 'Korczak', '777 009 999');
insert into personal_data values (11, 'Marian', 'Przysiężny', '777 001 999');
insert into personal_data values (12, 'Paulina', 'Darowicz', '777 004 999');


commit;

-- Parents
insert into parent values (1, 1);
insert into parent values (2, 3);
insert into parent values (3, 5);
insert into parent values (4, 7);

commit;

-- Teachers
insert into teacher values(1, 10);
insert into teacher values(2, 11);
insert into teacher values(3, 12);

commit;

-- student
insert into student values (1, 2, 1); -- tomek b
insert into student values (2, 4, 2); -- lucja
insert into student values (3, 6, 3); -- julia
insert into student values (4, 8, 4); -- basia

commit;

-- class
insert into "class" values (1, '8B', 1, 1);
insert into "class" values (2, '8B', 1, 2);
insert into "class" values (3, '8B', 1, 3);
insert into "class" values (4, '8B', 1, 4);

commit;

-- subject
insert into subject values (1, 'Biologia', 1);
insert into subject values (2, 'Matematyka', 2);
insert into subject values (3, 'Język polski', 3);

commit;

-- comment
insert into "comment" values (1, 'Odpowiedź ustna', '2020-03-04', 10);
insert into "comment" values (2, 'Brak przygotowania', '2020-03-06', 10);
insert into "comment" values (3, 'Widać, że się uczył uczeń', '2020-03-06', 10);

commit;

-- grade
insert into grade (id, value, set_at, last_modified, comment_id, student_id, subject_id, teacher_id ) values (1, 4.5, '2020-01-12', '2020-01-12', 1, 1, 1, 1);
insert into grade (id, value, set_at, last_modified, comment_id, student_id, subject_id, teacher_id ) values (2, 1.0, '2020-01-12', '2020-01-12', 2, 2, 1, 2);
insert into grade (id, value, set_at, last_modified, comment_id, student_id, subject_id, teacher_id ) values (3, 5.0, '2020-01-12', '2020-01-12', 3, 3, 1, 3);

commit;


