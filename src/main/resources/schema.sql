drop table bad_report_card;
drop table time_table_row ;
drop table grade;
drop table subject ;
drop table "class" ;
drop table student;
drop table "comment" ;
drop table parent;
drop table teacher;
drop table personal_data;



create table PERSONAL_DATA(
                              ID bigserial primary key not null,
                              FIRST_NAME VARCHAR(31) not null,
                              LAST_NAME VARCHAR(63) not null,
                              PHONE_NUMBER VARCHAR(31)
);


create table COMMENT(
                        ID bigserial primary key not null,
                        content VARCHAR(320) not null,
                        CREATED_AT TIMESTAMP not null,
                        author_id int8 references personal_data(ID)
);

create table PARENT (
                        ID bigserial primary key not null,
                        PERSONAL_DATA_ID INT8 references PERSONAL_DATA(ID)
);

create table TEACHER (
                         ID bigserial primary key not null,
                         PERSONAL_DATA_ID INT8 references PERSONAL_DATA(ID)
);


create table STUDENT (
                         ID bigserial primary key not null,
                         PERSONAL_DATA_ID INT8 references PERSONAL_DATA(ID),
                         PARENT_ID INT8 references PARENT(ID)
);

create table CLASS (
                       ID bigserial primary key not null,
                       NAME varchar(3) not null,
                       TEACHER_ID INT8 references TEACHER(ID),
                       STUDENT_ID INT8 references STUDENT(ID)
);

create table SUBJECT (
                         ID bigserial primary key not null,
                         NAME VARCHAR(63) not null,
                         TEACHER_ID INT8 references TEACHER(ID)
);

create table GRADE (
                       ID bigserial primary key not null,
                       VALUE FLOAT not null,
                       SET_AT TIMESTAMP not null,
                       LAST_MODIFIED TIMESTAMP not null,
                       TEACHER_ID INT8 references TEACHER(ID),
                       STUDENT_ID INT8 references STUDENT(ID),
                       SUBJECT_ID INT8 references SUBJECT(ID),
                       COMMENT_ID INT8 references COMMENT(ID) on delete cascade on update cascade
);

create table TIME_TABLE_ROW (
                                ID bigserial primary key not null,
                                WEEK_DAY_ISO INTEGER not null,
                                TIME TIME not null,
                                CLASS_ID INT8 references CLASS(ID) not null
);



create table BAD_REPORT_CARD(
                                ID bigserial primary key not null,
                                SET_AT TIMESTAMP not null,
                                SEEN_BY_PARENTS BOOLEAN not null,
                                COMMENT_ID INT8 references COMMENT(ID),
                                STUDENT_ID INT8 references STUDENT(ID),
                                TEACHER_ID INT8 references TEACHER(ID)
);




create view grade_view as
select g.id, g.value as value, subject.name as subject, concat(student_personal.first_name, ' ', student_personal.last_name) as student,
       concat(teacher_personal.first_name, ' ', teacher_personal.last_name) as teacher, comm.content as comment, g.set_at as setAt,
       comm.id as commentId, comm.author_id as commentAuthorId
from grade g
         join subject on g.subject_id = subject.id
         join student on g.student_id = student .id
         join personal_data student_personal on student.id = student_personal.id
         join teacher on g.teacher_id = teacher.id
         join personal_data teacher_personal on teacher.personal_data_id = teacher_personal.id
         join comment comm on g.comment_id = comm.id
;

create view student_view as
select student.id, concat(personal_data.first_name, ' ', personal_data.last_name) as name
from student
         join personal_data on student.personal_data_id  = personal_data.id


create view teacher_view as
select teacher.id, concat(personal_data.first_name, ' ', personal_data.last_name) as name
from teacher
         join personal_data on teacher.personal_data_id  = personal_data.id
;

create view subject as
select subject.id, subject.name
from subject